#coding:utf-8

import urllib,httplib,os,time,urllib2
import others
import time,json,threading

class MyQQ:
    myCookies={
    'pgv_pvi': '2518234112',
    'pgv_si': 's2264102912',
    }
    myParams={
        'vfwebqq': '',
        'vfwebqq2': '',
        'psessionid': '',
        'uin': ''
        }
    friends={
        "friends": '',
        "marknames": '',
        "categories":'',
        "vipinfo":  '',
        "info": ''
        }
    groups={
        
        }
    discussL={
        
        }
    online_buddies={}
    recent_list={}
    cookieFilePath='cookie.txt'

    conns={}

    rcvFunc=None
    
    def __init__(self):
        nothing=True

    def login(self,newUser=False):
        #read from disk
        #免除扫码的麻烦
        
        if newUser==False and self.readCookieFromFile() and self.downloadData():#成功
            return True
        else:#失败，重新扫码
            if self.ptqrlogin():#扫码成功
                self.writeCookieToFile()
                if self.downloadData():
                    return True
                else:
                    return False
            else:
                return False

    def close():
        self.isClosed=True
        for c in self.conns:
            c.close()
    
    def readCookieFromFile(self):
        if os.path.exists(self.cookieFilePath):
            f=open(self.cookieFilePath, 'r')
            c_j=f.read()
            self.myCookies=json.loads(c_j)
            f.close()
            return True
        return False
    def writeCookieToFile(self):
        c_j=json.dumps(self.myCookies)
        f=open(self.cookieFilePath,'w')
        f.write(c_j)
        f.close()
    
    def get_time(self):
        t=time.time()
        return (int(round(t * 1000)))

    def set_rcvFunc(self,rcvFunc):
        self.rcvFunc=rcvFunc
        
    def sendToServerPost(self,server,url,_cookie,referer,data):#向服务器发送数据POST
        cookie=others.prod_cookie(_cookie, self.myCookies)
        headers={
            'Content-Type': 'application/x-www-form-urlencoded',
            'cookie': cookie,
            'Referer': referer,
            'connection': 'keep-alive'
            }
        data_encode=urllib.urlencode(data)
        if not self.conns.has_key(server):
            self.conns[server]=httplib.HTTPConnection(server)
        conn=self.conns[server]
        conn.request(url=url, method='POST', headers=headers, body=data_encode)
        res=conn.getresponse().read()
        res_j={}
        try:
            res_j=json.loads(res)
        except BaseException:
            nothing=True
        #conn.close()
        return res_j

    def sendToServerGet(self,server,url,_cookie,referer):#向服务器发送数据GET
        cookie=others.prod_cookie(_cookie, self.myCookies)
        headers={
            'cookie': cookie,
            'Referer': referer,
            'connection': 'keep-alive'
            }
        if not self.conns.has_key(server):
            self.conns[server]=httplib.HTTPConnection(server)
        conn=self.conns[server]
        conn.request(url=url, method='GET', headers=headers)
        res=conn.getresponse().read()
        res_j={}
        try:
            res_j=json.loads(res)
        except BaseException:
            nothing=True
        #conn.close()
        return res_j

    def downloadData(self):#下载数据
        if self.get_params() \
        and self.get_user_friend2() \
        and self.get_group_name_list_mask2() \
        and self.get_discus_list() \
        and self.get_online_buddies() \
        and self.get_recent_list() \
        :
            #开启接收消息线程
            self.poll2Thread=threading.Thread(target=self.poll2,args=())
            self.poll2Thread.setDaemon(True)
            self.poll2Thread.start()
            return True
        return False
    
    def ptqrlogin(self):#获取二维码，发送判断登录数据包，成功后，更新cookie
        qrcode_url='https://ssl.ptlogin2.qq.com/ptqrshow?appid=501004106&e=2&l=M&s=3&d=72&v=4&t=0.4730401854383024&daid=164'
        qrcode_jpg='QRCode.jpg'
        response=urllib.urlretrieve(qrcode_url, qrcode_jpg)
        setCookie=response[1]['Set-Cookie']
        qrsig_tmp=setCookie.split(';')[0]
        qrsig=qrsig_tmp.split('=')[1]
        #print qrsig
        
        os.system(qrcode_jpg)

        login_server='ssl.ptlogin2.qq.com'
        login_url='https://ssl.ptlogin2.qq.com/ptqrlogin?u1=http%3A%2F%2Fw.qq.com%2Fproxy.html&ptredirect=0&h=1&t=1&g=1&from_ui=1&ptlang=2052&action=2-0-1503747252956&js_ver=10228&js_type=1&login_sig=&pt_uistyle=40&aid=501004106&daid=164&mibao_css=m_webqq&'
        
        login_url+='ptqrtoken='+others.get_ptqrtoken(qrsig)+'&'
        #print login_url
        headers={
            'cookie': 'qrsig='+qrsig,
            'referer': 'https://xui.ptlogin2.qq.com/cgi-bin/xlogin?daid=164&target=self&style=40&mibao_css=m_webqq&appid=501004106&enable_qlogin=0&no_verifyimg=1&s_url=http%3A%2F%2Fw.qq.com%2Fproxy.html&f_url=loginerroralert&strong_login=1&login_state=10&t=20131024001'
            }
        #print headers 
        login_conn=httplib.HTTPConnection(login_server)
        for i in range(0,30):
            login_conn.request(url=login_url, method="GET", headers=headers)
            response=login_conn.getresponse()
            res=response.read()
            code=others.logintype(res)
            if code==1:#成功
                print 'login successful'
                headers=response.getheaders()
                others.readlist(headers, self.myCookies)
                #print self.myCookies
                login_conn.close()

                #check_sig
                _sig_cookie=['RK','pt2gguin','ptcz','ptisp','skey','uin','pgv_pvi','pgv_si']
                sig_cookie=others.prod_cookie(_sig_cookie, self.myCookies)
                sig_headers={
                    'cookie': sig_cookie
                    }
                sig_server='ptlogin2.web2.qq.com'
                sig_url=res.split('\'')[5].split('http://ptlogin2.web2.qq.com')[1]
                #print sig_url
                
                sig_conn=httplib.HTTPConnection(sig_server)
                sig_conn.request(url=sig_url, method='GET', headers=sig_headers)
                sig_resp=sig_conn.getresponse()
                #print sig_resp.read()
                headers=sig_resp.getheaders()
                others.readlist(headers, self.myCookies)
                #print self.myCookies
                
                return True
            elif code==0:#验证中
                print 'isvalidating'
            else:
                print 'QR code is valid'
            time.sleep(2)
        login_conn.close()
        return False
    
    def get_params(self):
        #getvfwebqq
        vf_ser='s.web2.qq.com'
        vf_url='/api/getvfwebqq?ptwebqq=&clientid=53999199&psessionid=&t='
        vf_url+=str(self.get_time())
        _vf_cookie=['RK','p_skey','p_uin','pgv_pvi','pgv_si','pt2gguin','pt4_token','ptcz','ptisp','skey','uin']
        vf_referer='http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1'
        
        vf_res_f=self.sendToServerGet(vf_ser,vf_url,_vf_cookie,vf_referer)
        if vf_res_f['retcode']!=0:
            print 'getvfwebqq error'
            return False
        self.myParams['vfwebqq']=vf_res_f['result']['vfwebqq']
        #print self.myParams

        #psessionid
        #vfwebqq(2)
        #uin
        login2_ser='d1.web2.qq.com'
        login2_url='/channel/login2'
        _login2_cookie=['pgv_pvi','pgv_si','ptisp','RK','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        login2_referer='http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2'
        login2_data={
            'r': '{"ptwebqq":"","clientid":53999199,"psessionid":"","status":"online"}'
            }
        
        login2_res_j=self.sendToServerPost(login2_ser,login2_url,_login2_cookie,login2_referer,login2_data)
        
        if login2_res_j['retcode']!=0:
            print 'login2 error'
            return False
        self.myParams['psessionid']=login2_res_j['result']['psessionid']
        self.myParams['vfwebqq2']=login2_res_j['result']['vfwebqq']
        self.myParams['uin']=login2_res_j['result']['uin']
        #print self.myParams

        return True
   
    def get_user_friend2(self):
        #get_user_friend2
        server='s.web2.qq.com'
        url='/api/get_user_friends2'
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1'
        data={
            'r': '{"vfwebqq":"'+self.myParams['vfwebqq']+'","hash":"'+others.get_friend_hash(self.myParams['uin'],self.myParams['vfwebqq2'])+'"}'
            }
        res_j=self.sendToServerPost(server,url,_cookie,referer,data)
        if res_j['retcode']!=0 :
            print 'get_user_friend2 error'
            return False
        self.friends=res_j['result']
        return True
    
    def get_group_name_list_mask2(self):
        server='s.web2.qq.com'
        url='/api/get_group_name_list_mask2'
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1'
        data={
            'r': '{"vfwebqq":"'+self.myParams['vfwebqq']+'","hash":"'+others.get_friend_hash(self.myParams['uin'],self.myParams['vfwebqq2'])+'"}'
            }
        res_j=self.sendToServerPost(server,url,_cookie,referer,data)
        if res_j['retcode']!=0:
            print 'get_group_name_list_mask2 error'
            return False
        self.groups=res_j['result']
        return True
    
    def get_discus_list(self):
        server='s.web2.qq.com'
        url='/api/get_discus_list?clientid=53999199'
        url+='&psessionid='+self.myParams['psessionid']+'&vfwebqq='+self.myParams['vfwebqq']+'&t='+str(self.get_time())
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1'

        res_j=self.sendToServerGet(server,url,_cookie,referer)
        if res_j['retcode']!=0:
            print 'get_discus_list error'
            return False
        self.discussL=res_j['result']
        return True

    def get_online_buddies(self):
        server='d1.web2.qq.com'
        url='/channel/get_online_buddies2?'
        url+='vfwebqq='+self.myParams['vfwebqq']+'&clientid=53999199&psessionid='+self.myParams['psessionid']+'&t='+str(self.get_time())
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2'

        res_j=self.sendToServerGet(server,url,_cookie,referer)
        if res_j['retcode']!=0:
            print 'get_online_buddies error'
            return False
        self.online_buddies=res_j['result']
        return True

    def get_recent_list(self):
        server='d1.web2.qq.com'
        url='/channel/get_recent_list2'
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2'
        _data={
            "vfwebqq": self.myParams['vfwebqq'],
            "clientid": 53999199,
            "psessionid": self.myParams['psessionid']
            }
        _data_j=json.dumps(_data)
        data={
            'r': _data_j
            }
        res_j=self.sendToServerPost(server,url,_cookie,referer,data)
        if res_j['retcode']!=0:
            print 'get_recent_list error'
            return False
        self.recent_list=res_j['result']
        return True
    
    def poll2(self):
        server='d1.web2.qq.com'
        url='/channel/poll2'
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2'
        headers={
            'Content-Type': 'application/x-www-form-urlencoded',
            'cookie': others.prod_cookie(_cookie,self.myCookies),
            'Referer': referer,
            'connection': 'keep-alive'
            }
        _data={
            "ptwebqq":"",
            "clientid":53999199,
            "psessionid":self.myParams['psessionid'],
            "key":""
        }
        _data_j=json.dumps(_data)
        data={
            'r': _data_j
            }
        data_encode=urllib.urlencode(data)
        conn=httplib.HTTPConnection(server)
        cnt=0
        while not self.isClosed:
            #_f=open('poll'+str(cnt)+'.txt','w')
            try:
                conn.request(url=url,method='POST',headers=headers,body=data_encode)
                resp=conn.getresponse()
                res=resp.read()
                res_j=json.loads(res)
                print res_j
                if not res_j.has_key('errmsg'):
                    if not rcvFunc:
                        rcvFunc()
                    #self.recv_msg(res_j)
                    
                #_f.write(resp.getheaders())
            except Exception:
                #_f.write('error')
                print 'error'
                nothing=True
            finally:
                cnt=cnt+1
            #_f.close()
        conn.close()

    def t():
        print 'ok'
        return 1
    
    def recv_msg(res_j):
        print 'rcv'
        result=res_j['result'][0]['value']
        msg_type=result['msg_type']
        from_uin=result['from_uin']
        to_uin=result['to_uin']
        content=result['content'][1]#消息内容
        print content
        
    def cfproxy(self):
        server='d1.web2.qq.com'
        url='/cfproxy.html?v=20151105001&callback=1'
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://w.qq.com/'
        self.sendToServerGet(server,url,_cookie,referer)

    s_cfproxy=False
    isClosed=False
    
    def get_uin_by_name(self,name):
        fi=self.friends['info']
        uin=-1
        for _f in fi:
            if _f['nick']==name:
                if uin!=-1:#not only
                    return -1
                uin=_f['uin']
        return uin
    
    def send_msg(self,uin,msg):
#        if self.s_cfproxy==False:
#            self.s_cfproxy=True
#            self.poll2()
#            self.cfproxy()
        msg=str(msg)
        msg=msg.decode('gbk')
        server='d1.web2.qq.com'
        url='/channel/send_buddy_msg2'
        _cookie=['pgv_pvi','RK','pgv_si','ptisp','ptcz','pt2gguin','uin','skey','p_uin','p_skey','pt4_token']
        referer='http://d1.web2.qq.com/cfproxy.html?v=20151105001&callback=1'
        __data={
            "to":uin,
            "content": '["'+msg+'",["font",{"name":"\u5b8b\u4f53","size":10,"style":[0,0,0],"color":"000000"}]]',
                        #",["font",{"name":"宋体","size":10,"style":[0,0,0],"color":"000000"}]]'
            "face":0,
            "clientid":53999199,
            "msg_id":0,
            "psessionid": self.myParams['psessionid']
            }
        __data_j=json.dumps(__data)
        data={
            'r': __data_j
            }
        res_f=self.sendToServerPost(server,url,_cookie,referer,data)
        print res_f
        print 'send message successful'

    poll2Thread=None
