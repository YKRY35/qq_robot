#encoding:utf-8
def logintype(s):
    code=int(s.split('\'')[1])
    if(code==0):
        return 1
    elif(code==66):
        return -1
    else:
        return 0


def get_ptqrtoken(s):
    res=0
    l=len(s)
    for i in range(0,l):
        res+=(res<<5)+ord(s[i])
    return str(2147483647 & res)


def byte2hex(_b):
    _h=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    buf=''
    for i in range(0,len(_b)):
        buf+=(_h[(_b[i]>>4)&0xF])
        buf+=(_h[_b[i]&0xF])
    return buf

def get_friend_hash(uin, ptvfwebqq):
    if uin=='':
        return ''
    ptvfwebqq+=ptvfwebqq
    ptb=[0,0,0,0]
    for i in range(0,len(ptvfwebqq)):
        ptbIndex=i%4
        ptb[ptbIndex]^=ord(ptvfwebqq[i])
        
    salt=['EC','OK']
    uinByte=[0,0,0,0]
    uinByte[0] = (((uin >> 24) & 0xFF) ^ ord(salt[0][0]))
    uinByte[1] = (((uin >> 16) & 0xFF) ^ ord(salt[0][1]))
    uinByte[2] = (((uin >> 8) & 0xFF) ^ ord(salt[1][0]))
    uinByte[3] = ((uin & 0xFF) ^ ord(salt[1][1]))

    result = [0,0,0,0,0,0,0,0]
    for i in range(0,8):
        if i%2==0 :
            result[i] = ptb[i>>1]
        else:
            result[i] = uinByte[i>>1]
    return byte2hex(result);

def readstr(s, _c):
    cookie=s.split(')')[1].split('(')[1].split('\'')[3]
    oneC=cookie.split(';, ')
    _oneC=[]
    for _o in oneC:
        __o= _o.split('; HttpOnly, ')
        for ___o in __o:
            _oneC.append(___o)
    oneC=_oneC
    for _o in oneC:
        cc=_o.split(';')[0]
        _c[cc.split('=')[0]]=cc.split('=')[1]
    print _c

def readlist(l, _c):
    oneC=l[1][1].split(';, ')
    _oneC=[]
    for _o in oneC:
        __o= _o.split('; HttpOnly, ')
        for ___o in __o:
            _oneC.append(___o)
    oneC=_oneC
    for _o in oneC:
        cc=_o.split(';')[0]
        le=cc.split('=')[0]
        ri=cc.split('=')[1]
        if ri=='':
            continue
        _c[le]=ri

def prod_cookie(l, c):#list cookies
    res=''
    first=True
    for o in l:
        if first==False:
            res+='; '
        res+=o+'='+c[o]
        first=False
    return res
